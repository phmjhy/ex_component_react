import React, { Component } from "react";
import Content from "./Content";
import Footer from "./Footer";
import Header from "./Header";
import Welcome from "./Welcome";

export default class Layout extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Welcome />
        <Content />
        <Footer />
      </div>
    );
  }
}
